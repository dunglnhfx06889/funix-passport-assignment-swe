# FUNiX Passport
FUNiX Passport
Mã học viên: FX06889
Repo này được sử dụng để quản lý và phát triển một dự án có tên là FUNiX Passport, một Extension cho trình duyệt, cho phép người dùng xem phụ đề trên các video từ nhiều nguồn khác nhau, bao gồm Udemy.

Có các branch sau:

- master : Đây là branch chính của dự án, nơi các bản release chính thức được tạo ra.

- bug/clear_console_log : Branch này được tạo để xử lý việc loại bỏ tất cả các câu lệnh console.log ra khỏi file udemy-subtitle.js. Điều này nhằm giảm bớt số lượng log không cần thiết, làm tăng hiệu suất ứng dụng.

- feat/auto_enable_subtitle : Branch này được tạo để thêm một tính năng mới: tự động hiển thị phụ đề nếu có.

- document : Branch này dùng để quản lý việc tạo và cập nhật tài liệu dự án.

- backend : Branch này chứa thông tin và source code của Backend và tài liệu cho Backend.